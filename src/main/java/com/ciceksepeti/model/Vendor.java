package com.ciceksepeti.model;

import com.ciceksepeti.app.HackathonConstant;

public class Vendor {
	private String color;
	private Point point;
	private Integer minOrderNumber;
	private Integer maxOrderNumber;

	public Vendor() {
	}

	public Vendor(String color, Point point) {
		this.color = color;
		this.point = point;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		if (color.equals(HackathonConstant.RED_VENDOR_NAME)) {
			setMinOrderNumber(HackathonConstant.RED_VENDOR_MIN_ORDER_NUM);
			setMaxOrderNumber(HackathonConstant.RED_VENDOR_MAX_ORDER_NUM);
		} else if (color.equals(HackathonConstant.GREEN_VENDOR_NAME)) {
			setMinOrderNumber(HackathonConstant.GREEN_VENDOR_MIN_ORDER_NUM);
			setMaxOrderNumber(HackathonConstant.GREEN_VENDOR_MAX_ORDER_NUM);
		} else if (color.equals(HackathonConstant.BLUE_VENDOR_NAME)) {
			setMinOrderNumber(HackathonConstant.BLUE_VENDOR_MIN_ORDER_NUM);
			setMaxOrderNumber(HackathonConstant.BLUE_VENDOR_MAX_ORDER_NUM);
		}
		this.color = color;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public Integer getMinOrderNumber() {
		return minOrderNumber;
	}

	public void setMinOrderNumber(Integer minOrderNumber) {
		this.minOrderNumber = minOrderNumber;
	}

	public Integer getMaxOrderNumber() {
		return maxOrderNumber;
	}

	public void setMaxOrderNumber(Integer maxOrderNumber) {
		this.maxOrderNumber = maxOrderNumber;
	}

	@Override
	public String toString() {
		return "Vendor [color=" + color + ", lag=" + point.getLatitude() + ", longitude=" + point.getLongitude()
				+ ", minOrderNumber=" + minOrderNumber + ", maxOrderNumber=" + maxOrderNumber + "]";
	}

}
