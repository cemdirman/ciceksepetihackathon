package com.ciceksepeti.model;

public class Order {
	private Integer orderID;
	private Point point;
	private Boolean hasVendor;

	public Order() {
	}

	public Order(Integer orderID, Point point) {
		this.orderID = orderID;
		this.point = point;
	}

	public Integer getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = Integer.valueOf(orderID);
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public Boolean getHasVendor() {
		return hasVendor;
	}

	public void setHasVendor(Boolean hasVendor) {
		this.hasVendor = hasVendor;
	}

	@Override
	public String toString() {
		return "Order [orderID=" + orderID + ", latitude=" + point.getLatitude() + ", longitude=" + point.getLongitude()
				+ ", hasVendor=" + hasVendor + "]";
	}

}
