package com.ciceksepeti.model;

public class Point {
	private Double latitude;
	private Double longitude;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = Double.valueOf(latitude);
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = Double.valueOf(longitude);
	}
}
