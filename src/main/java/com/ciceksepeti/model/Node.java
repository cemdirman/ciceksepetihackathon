package com.ciceksepeti.model;

import java.util.HashMap;
import java.util.Map;

public class Node {

	private Integer orderID;
	private Map<Integer, Map<Vendor, Double>> orderDistanceMap = new HashMap<>();

	public Integer getOrderID() {
		return orderID;
	}

	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}

	public Map<Integer, Map<Vendor, Double>> getOrderDistanceMap() {
		return orderDistanceMap;
	}

	@Override
	public String toString() {
		return "Node [orderID=" + orderID + ", orderDistanceMap=" + orderDistanceMap + "]";
	}

}
