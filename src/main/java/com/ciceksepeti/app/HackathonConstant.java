package com.ciceksepeti.app;

public interface HackathonConstant {

	public static String RED_VENDOR_NAME = "Kırmızı ";
	public static String GREEN_VENDOR_NAME = "Yeşil";
	public static String BLUE_VENDOR_NAME = "Mavi";

	public static Integer RED_VENDOR_MIN_ORDER_NUM = 20;
	public static Integer RED_VENDOR_MAX_ORDER_NUM = 30;
	public static Integer GREEN_VENDOR_MIN_ORDER_NUM = 35;
	public static Integer GREEN_VENDOR_MAX_ORDER_NUM = 50;
	public static Integer BLUE_VENDOR_MIN_ORDER_NUM = 20;
	public static Integer BLUE_VENDOR_MAX_ORDER_NUM = 80;

}
