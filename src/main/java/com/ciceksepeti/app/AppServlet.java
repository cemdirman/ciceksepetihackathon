package com.ciceksepeti.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ciceksepeti.model.Node;
import com.ciceksepeti.model.Order;
import com.ciceksepeti.model.Point;
import com.ciceksepeti.model.Vendor;
import com.ciceksepeti.util.CalculateUtil;
import com.ciceksepeti.util.FileUtil;

@Controller("order")
public class AppServlet implements InitializingBean {

	private static List<Order> orderList;
	private static List<Vendor> vendorList;

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Order controller has been initialized");
		readExcel();
		writeLists();
	}

	private void readExcel() {
		FileUtil util;
		try {
			util = new FileUtil("koordinatlar.xlsx");
			orderList = util.getOrderList();
			vendorList = util.getVendorList();
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			// TODO LOG
			e.printStackTrace();
		}
	}

	@RequestMapping("map")
	public List<Node> getOrderVendor() {
		return prapereOrderVendorList();
	}

	private List<Node> prapereOrderVendorList() {
		List<Node> nodeList = new ArrayList<>();
		int counter = 0;
		while (counter < orderList.size()) {
			Node node = new Node();
			Order order = orderList.get(counter);
			node.setOrderID(order.getOrderID());
			Map<Vendor, Double> orderDistanceMap = getOrderDistanceMap(order);
			// TODO:kapasiteye göre kontrol eklenmeli. eger kapasite sondaysa bir sonraki en
			// kısa mesafa ve bayi numarası bulunmalı.
			// TODO:controlVendorCapacity() : boolean -> bayinin kapasite sınırlarında ise
			// ekle değilse diğer en yakın bayiyi bul
			node.getOrderDistanceMap().put(order.getOrderID(), orderDistanceMap);
			order.setHasVendor(true);
			nodeList.add(node);
			System.out.println(node.toString());
			counter++;
		}
		return nodeList;
	}

	private Map<Vendor, Double> getOrderDistanceMap(Order order) {
		Map<Vendor, Double> vendorDistanceMap = new HashMap<>();
		double[] distanceList = new double[3];
		Vendor vendor;
		for (int i = 0; i < 3; i++) {
			vendor = vendorList.get(i);
			distanceList[i] = calculateDistance(order.getPoint(), vendor.getPoint());
		}
		double[] minDistanceAndVendorIndex = findMinDistanceAndVendorIndex(distanceList);
		double minDistance = minDistanceAndVendorIndex[0];
		int vendorIndex = (int) minDistanceAndVendorIndex[1];
		vendorDistanceMap.put(vendorList.get(vendorIndex), minDistance);
		return vendorDistanceMap;
	}

	private double calculateDistance(Point orderPoint, Point vendorPoint) {
		return CalculateUtil.calculate(orderPoint, vendorPoint);
	}

	/**
	 * @param distanceList
	 *            has each vendor distance
	 * @return min distance and whic vendor has min distance
	 */
	private double[] findMinDistanceAndVendorIndex(double[] distanceList) {
		double minDistance = distanceList[0];
		double index = 0;
		for (int i = 0; i < distanceList.length; i++) {
			if (distanceList[i] < minDistance) {
				minDistance = distanceList[i];
			}
		}
		return new double[] { minDistance, index };
	}

	private static void writeLists() {
		System.out.println("-----------------Order List----------------------");
		for (Order order : orderList) {
			System.out.println(order.toString());
		}
		System.out.println("-----------------Vendor List---------------------");
		for (Vendor vendor : vendorList) {
			System.out.println(vendor.toString());
		}
		System.out.println("-------------------------------------------------");
	}

}
