package com.ciceksepeti.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.ciceksepeti.model.Order;
import com.ciceksepeti.model.Point;
import com.ciceksepeti.model.Vendor;

public class FileUtil {
	private Workbook workbook;

	public FileUtil(String filePath) throws EncryptedDocumentException, InvalidFormatException, IOException {
		workbook = WorkbookFactory.create(getFile(filePath));
		getSheets();
	}

	public List<Order> getOrderList() throws IOException {
		List<Order> orderList = new ArrayList<>();
		Sheet firstSheet = workbook.getSheetAt(0);
		for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {
			Row row = firstSheet.getRow(i);
			Iterator<Cell> cellIterator = row.cellIterator();
			Order o = new Order();
			Point p = new Point();
			while (cellIterator.hasNext()) {
				Cell nextCell = cellIterator.next();
				int columnIndex = nextCell.getColumnIndex();
				switch (columnIndex) {
				case 0:
					o.setOrderID(nextCell.getStringCellValue());
					break;
				case 1:
					p.setLatitude(nextCell.getStringCellValue());
					break;
				case 2:
					p.setLongitude(nextCell.getStringCellValue());
					break;
				}
			}
			o.setPoint(p);
			orderList.add(o);
		}
		return orderList;
	}

	public List<Vendor> getVendorList() throws IOException {
		List<Vendor> vendorList = new ArrayList<>();
		Sheet firstSheet = workbook.getSheetAt(1);
		for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {
			Row row = firstSheet.getRow(i);
			Iterator<Cell> cellIterator = row.cellIterator();
			Vendor v = new Vendor();
			Point p = new Point();
			while (cellIterator.hasNext()) {
				Cell nextCell = cellIterator.next();
				int columnIndex = nextCell.getColumnIndex();
				switch (columnIndex) {
				case 0:
					v.setColor(nextCell.getStringCellValue());
					break;
				case 1:
					p.setLatitude(nextCell.getStringCellValue());
					break;
				case 2:
					p.setLongitude(nextCell.getStringCellValue());
					break;
				}
			}
			v.setPoint(p);
			vendorList.add(v);
		}
		return vendorList;
	}

	private void getSheets() {
		System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
		Iterator<Sheet> sheetIterator = workbook.sheetIterator();
		while (sheetIterator.hasNext()) {
			Sheet sheet = sheetIterator.next();
			System.out.println("=> " + sheet.getSheetName());
		}
	}

	public File getFile(String path) {
		ClassLoader classLoader = getClass().getClassLoader();
		return new File(classLoader.getResource(path).getFile());
	}

}
