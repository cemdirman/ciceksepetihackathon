package com.ciceksepeti.util;

import com.ciceksepeti.model.Point;

public class CalculateUtil {

	private CalculateUtil() {
	}

	public static Double calculate(Point p1, Point p2) {
		return Math.hypot(p1.getLatitude() - p2.getLatitude(), p1.getLongitude() - p2.getLongitude());
	}

}
